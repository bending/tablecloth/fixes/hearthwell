package hwell;

import com.gamerforea.eventhelper.util.EventUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;

public class BukkitHelper {
    private static boolean isTableclothServer = false;

    static {
        try {
            Class.forName("tablecloth.server.TableclothServer");
            isTableclothServer = true;
        } catch (ClassNotFoundException e) {
        }
    }
    public static boolean canBreak(EntityPlayer player, BlockPos pos) {
        if (!isTableclothServer)
            return true;
        return !EventUtils.cantBreak(player, pos);
    }

}
